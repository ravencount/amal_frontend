FROM node:16-slim as build
WORKDIR /app
COPY . /app/
RUN npm i 
RUN npm i -g react-scripts@4.0.3
RUN npm run build


FROM nginx:1.21.3-alpine 
COPY --from=build /app/build /usr/share/nginx/html
RUN rm /etc/nginx/conf.d/default.conf
COPY .nginx/nginx.conf /etc/nginx/conf.d 
EXPOSE 80 
CMD ["nginx", "-g", "daemon off;"]